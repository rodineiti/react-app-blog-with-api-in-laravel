import React from "react";
import { BrowserRouter } from "react-router-dom";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Routes from "./Routes";

export default function App() {
  return (
    <>
      <BrowserRouter>
        <Header visible={sessionStorage.getItem("meutoken") || null} />
        <Routes />
        <Footer />
      </BrowserRouter>
    </>
  );
}
