import React from "react";
import Item from "./Item";
import Search from "./Search";
import Category from "./Category";

export default function Posts({ title, posts, categories, search, read }) {
  return (
    <div className="row">
      <div className="col-md-8">
        <h1 className="my-4">{title || ""}</h1>

        {posts.length === 0 && (
          <div className="alert alert-info">Nenhuma postagem no momento</div>
        )}

        {posts.length > 0 &&
          posts.map((item, key) => <Item read={read} key={key} post={item} />)}
      </div>

      <div className="col-md-4">
        <Search search={search} />
        <Category items={categories} />
      </div>
    </div>
  );
}
