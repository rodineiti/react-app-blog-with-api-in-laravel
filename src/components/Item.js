import React from "react";
import { Link } from "react-router-dom";

export default class Item extends React.Component {
  render() {
    const { post, read } = this.props;
    const { comments = [] } = post;
    const pathImage =
      post.image === null ? "http://placehold.it/750x300" : post.image_url;
    return (
      <>
        <div className="card mb-4">
          <img
            className="card-img-top"
            src={`${pathImage}`}
            alt="Card image cap"
            title={"Card image cap"}
          />
          <div className="card-body">
            <h2 className="card-title">{post.title}</h2>
            <p className="card-text">{post.body}</p>
            {!read && (
              <Link to={`/posts/show/${post.id}`} className="btn btn-primary">
                Read More &rarr;
              </Link>
            )}
          </div>
          <div className="card-footer text-muted">
            Category{" "}
            <Link to={`/posts/category/${post.category_id}`}>
              {post.category.name}
            </Link>{" "}
            <br />
            Posted on January {post.created_at.toLocaleString("pt-BR")} by
            <Link to={`/posts/user/${post.user_id}`}> {post.user.name}</Link>
          </div>
        </div>

        <div className="card my-4">
            <h5 className="card-header">Leave a Comment:</h5>
            <div className="card-body">                    
                <form action="#">
                    <div className="form-group">
                        <textarea className="form-control" rows="3" name="body"></textarea>
                    </div>
                    <button type="submit" className="btn btn-primary">Comment</button>
                </form>
            </div>
        </div>

        {comments.length > 0 && comments.map((comment, keyc) => {
          const { replies = [] } = comment;
          return (
            <div className="media mb-4" key={keyc}>
              <img className="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="" />
              <div className="media-body">
                <h5 className="mt-0">{comment.user.name}</h5>
                <p><i>{comment.created_at}</i></p>
                {comment.body}
                {replies.length > 0 && replies.map((reply, keyr) => (
                  <div className="media mt-4" key={keyr}>
                    <img className="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="" />
                    <div className="media-body">
                    <h5 className="mt-0">{reply.user.name}</h5>
                    <p><i>{reply.created_at}</i></p>
                    {reply.body}
                    </div>
                  </div>
                ))}

                <div className="card my-4">
                    <h5 className="card-header">Reply Comment:</h5>
                    <div className="card-body">                    
                        <form action="">
                            <div className="form-group">
                                <textarea className="form-control" rows="3" name="body"></textarea>
                            </div>
                            <button type="submit" className="btn btn-primary">Reply</button>
                        </form>
                    </div>
                </div>
              </div>
            </div>
          )
        })}
      </>
    );
  }
}
