import React, { useState } from "react";

export default function Search({ search }) {
  const [term, setTerm] = useState("");
  return (
    <div className="card my-4">
      <h5 className="card-header">Search</h5>
      <div className="card-body">
        <div className="input-group">
          <input
            type="text"
            className="form-control"
            placeholder="Search for..."
            value={term}
            onChange={(event) => setTerm(event.target.value)}
          />
          <span className="input-group-append">
            <button
              className="btn btn-secondary"
              type="button"
              onClick={() => search(term)}
            >
              Go!
            </button>
          </span>
        </div>
      </div>
    </div>
  );
}
