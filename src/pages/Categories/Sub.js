import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

export default function Sub() {
  const [name, setName] = useState("");
  const [category_id, setCategoryId] = useState("");
  const [message, setMessage] = useState("");
  const [categories, setCategories] = useState([]);
  const history = useHistory();

  useEffect(() => {
    getCategories();
  }, []);

  async function getCategories() {
    let config = {
      headers: {
        Authorization: "Bearer " + sessionStorage.getItem("meutoken"),
      },
    };

    const response = await axios.get(
      "http://localhost:8000/api/categories",
      config
    );
    setCategories(response.data);
  }

  async function onSubmit(event) {
    event.preventDefault();

    if (name !== "" && category_id !== "") {
      alert("Sub categoria adicionada");
    } else {
      setMessage("Prencha todos os campos");
    }
  }

  return (
    <div className="App">
      <div className="container mt-5 mb-5">
        <h1>Adicionar Sub Categoria</h1>
        <hr />
        {message !== "" && <div className="alert alert-info">{message}</div>}
        <form onSubmit={onSubmit}>
          <div className="form-group">
            <label htmlFor="category_id">Categoria</label>
            <select
              name="category_id"
              id="category_id"
              className="form-control"
              onChange={(event) => setCategoryId(event.target.value)}
            >
              {categories.map((item) => (
                <option value={item.id}>{item.name}</option>
              ))}
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              value={name}
              onChange={(event) => setName(event.target.value)}
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Add
          </button>
        </form>
      </div>
    </div>
  );
}
