import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import Posts from "./../components/Posts";

function FilterUser() {
  const [posts, setPosts] = useState([]);
  const [user, setUser] = useState(null);
  const [categories, setCategories] = useState([]);
  const { user_id } = useParams();

  useEffect(() => {
    pegarPosts();
  }, []);

  async function procurar(term = null) {
    if (term) {
      const response = await axios.get(
        `http://localhost:8000/api/posts?term=${term}`
      );
      const posts = response.data.posts;

      setPosts(posts);
    }
  }

  async function pegarPosts() {
    const response = await axios.get(
      `http://localhost:8000/api/posts/user/${user_id}`
    );

    const { posts, categories, user } = response.data;

    setPosts(posts);
    setUser(user);
    setCategories(categories);
  }

  return (
    <div className="App">
      <div className="container">
        <Posts
          title={`View Post By ${!user ? "" : user.name}`}
          posts={posts || []}
          categories={categories || []}
          search={procurar}
          read={false}
        />
      </div>
    </div>
  );
}

export default FilterUser;
