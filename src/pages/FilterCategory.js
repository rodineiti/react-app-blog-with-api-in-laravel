import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import Posts from "./../components/Posts";

function FilterCategory() {
  const [posts, setPosts] = useState([]);
  const [category, setCategory] = useState(null);
  const [categories, setCategories] = useState([]);
  const { category_id } = useParams();

  useEffect(() => {
    pegarPosts();
  }, [category_id]);

  async function procurar(term = null) {
    if (term) {
      const response = await axios.get(
        `http://localhost:8000/api/posts?term=${term}`
      );
      const posts = response.data.posts;

      setPosts(posts);
    }
  }

  async function pegarPosts() {
    const response = await axios.get(
      `http://localhost:8000/api/posts/category/${category_id}`
    );

    const { posts, categories, category } = response.data;

    setPosts(posts);
    setCategory(category);
    setCategories(categories);
  }

  return (
    <div className="App">
      <div className="container">
        <Posts
          title={`View Post By ${!category ? "" : category.name}`}
          posts={posts || []}
          categories={categories || []}
          search={procurar}
          read={false}
        />
      </div>
    </div>
  );
}

export default FilterCategory;
