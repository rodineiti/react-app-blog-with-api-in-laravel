import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

export default function Add() {
  const [name, setName] = useState("");
  const [message, setMessage] = useState("");
  const history = useHistory();

  async function onSubmit(event) {
    event.preventDefault();

    if (name !== "") {
      try {
        let config = {
          headers: {
            Authorization: "Bearer " + sessionStorage.getItem("meutoken"),
          },
        };

        await axios.post(
          "http://localhost:8000/api/categories",
          { name },
          config
        );

        history.push("/categories");
      } catch (error) {
        setMessage("Ocorreu um erro ao tentar adicionar categoria");
      }
    } else {
      setMessage("Prencha todos os campos");
    }
  }

  return (
    <div className="App">
      <div className="container mt-5 mb-5">
        <h1>Adicionar Categoria</h1>
        <hr />
        {message !== "" && <div className="alert alert-info">{message}</div>}
        <form onSubmit={onSubmit}>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              value={name}
              onChange={(event) => setName(event.target.value)}
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Add
          </button>
        </form>
      </div>
    </div>
  );
}
